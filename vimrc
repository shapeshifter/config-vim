" Basic "
scriptencoding utf-8                    " interpret this file as utf-8
set nocompatible                        " don't be vi-compatible
set noexrc                              " don't source /etc/vimrc
set autochdir                           " always switch to the current working dir
set backup                              " make backup files
set backupcopy=yes
set viminfo+=n~/.vim/viminfo            " where to store the session history
set backupdir=~/.vim/bak                " where to put backup files
set directory=~/.vim/swp                " where to put swap files
set undodir=~/.vim/undo/                " where to store undo history
set undofile                            " enable undo-after-restart support
set history=2000                        " store longer history
set fileformats=unix,dos,mac            " support all three, in this order
filetype plugin on                      " filetype detection
set linebreak

" Input "
set expandtab                           " pressing <TAB> writes spaces
set backspace=indent,eol,start          " allow backspace everywhere
let mapleader = ' '                     " remap "leader"
set mouse=a                             " enable mouse support
" Enter command line mode or history when typing a comma
nnoremap , :
nnoremap q, q:
nnoremap : <NOP>
nnoremap q: <NOP>
" Navigate previous commands using Ctrl-j/k in addition to arrow keys
cnoremap <C-k> <Up>
cnoremap <C-j> <Down>
" Replace selected string in document after confirmation for each occurence
vnoremap <C-r> "hy:%s/<C-r>h//gc<left><left><left>

" Appearance "
colorscheme molokaimodified
set noerrorbells                        " don't make noises
set novisualbell                        " don't flash
set ttyfast                             " faster redrawing
set lazyredraw                          " don't draw while running macros
set number                              " line numbers
set showcmd                             " show commands in status bar
set winminheight=0                      " windows can be hidden completely
set colorcolumn=81                      " vertical bar at this char width
syn on                                  " enable syntax hilighting
set hlsearch                            " highlight searches
set list listchars=tab:→\ ,trail:·      " show whitespace
set laststatus=2                        " always show status bar
set noea                                " don't resize all when closing a buffer

" Behavior "
set wildmode=list,full                  " bash-like tab completion in cl mode
set infercase                           " autocomplete case insensitive
set splitbelow                          " place new splits below current window
set scrolloff=4                         " scroll ahead for this many lines
set ignorecase                          " make searches case-insensitive
set smartcase                           " unless they contain upper-case letters
set incsearch                           " incremental search
set enc=utf-8                           " default encoding
set autoindent smartindent              " auto/smart indent
set expandtab                           " expand tabs to spaces
set smarttab                            " tab and backspace are smart
set wildignore+=*.DS_STORE,*.swc,*.dll,
  \*.o,*.so,*.obj,*.bak,*.exe,*.pyc,*.jpg,
  \*.gif,*.png                          " file endings to ignore
set nostartofline                       " don't move cursor to beginning of line
set shiftwidth=2                        " number of spaces used to indent
set tabstop=2                           " <TAB> character width

" Workarounds "
" ensure term bg color is reset on exit:
autocmd VimLeave * silent !echo -ne '\033[0m'

" Plugins "
" pathogen:
execute pathogen#infect()
execute pathogen#helptags()

" CtrlP:
" find files within assumed project dir
nnoremap <leader>f :CtrlP<CR>
" find files in and below cwd
nnoremap <leader>F :CtrlPCurWD<CR>
" find vim buffers
nnoremap <leader>b :CtrlPBuffer<CR>
" find in previously used files
nnoremap <leader>m :CtrlPMRU<CR>
" find string in open buffers
nnoremap <leader>s :CtrlPLine<CR>
" files to ignore
let g:ctrlp_custom_ignore = {
  \ 'dir':  '\v[\/]((target)[\/](streams|resolution-cache)|\.(git|hg|svn)$)',
  \ 'file': '\v\.(DS_STORE|swc|dll|o|so|obj|bak|exe|pyc|jpg|gif|png|class)$',
  \ }
" maximum number of files to search/cache
let g:ctrlp_max_files = 40000

" vim-easymotion
" search up and down
map <S-u> <leader><leader>b
map <S-d> <leader><leader>w

au VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces

let g:syntastic_java_javac_config_file_enabled = 1
let g:syntastic_ignore_files = ['\.py$']

" undotree
nnoremap <leader>u :UndotreeToggle<CR>

" au Filetype html,xml source ~/.vim/scripts/wrapwithtag.vim

autocmd BufEnter * let &titlestring = hostname() . "[vim(" . expand("%:t") . ")]"
