## Overview
This vim configuration sets some commonly used defaults and comes with a number
of plugins. It has the following noteworthy features over the defaults:

* It's intended for terminal vim (as opposed to gVim/MacVim) but it should work
  everywhere
* Stores backups and swap files in dedicated folders inside `~/.vim`
* Remembers undo history beyond vim restarts
* Uses spaces insted of tabs and makes tabs and trailing whitespace visible
* Uses ',' for entering command line mode instead of ':'
* Uses spacebar as `<Leader>`
* Enable mouse support
* Only uses vim script plugins, no other tools required except optional syntax
  checkers for syntastic

The following plugins are enabled as git submodules of this project, plus some
dependencies:

* [Pathogen](https://github.com/tpope/vim-pathogen): Needed for placing plugins
  in `bundle`
* [CtrlP](https://github.com/kien/ctrlp.vim): Fuzzy file searching
* [Syntastic](https://github.com/scrooloose/syntastic): Syntax checking for
  many source file types via any syntax checkers installed on the system
* [NERDTree](https://github.com/scrooloose/nerdtree): Hierarchical file browser
* [SnipMate](https://github.com/garbas/vim-snipmate): Tab-completion for
  snippets (e.g. function definitions, loops, etc.)
* [EasyMotion](https://github.com/Lokaltog/vim-easymotion): Jumping around in
  documents using just 3 key strokes
* [Rainbow-Parantheses](https://github.com/kien/rainbow_parentheses.vim):
  Colorize differently nested parantheses in different colors
* [NERD Commenter](https://github.com/scrooloose/nerdcommenter):
  Allow adding/removing block comments via a shortcut
* [undotree](https://github.com/mbbill/undotree): Visualize undo branches

The `vimrc` file contains comments on what the different options do.

## Installation
1.  Make sure your terminal supports 256 colors. If you're using rxvt-unicode
    for example, make sure your `$TERM` is correctly set by placing something
    like this in your `~/.Xdefaults`:

        urxvt*termName: rxvt-256color

    If you plan on using vim inside screen, make sure you have something like
    this in your `~/.screenrc`:

        term screen-256color

2.  Backup or Delete any existing `~/.vim*` files.
3.  Clone this config and all plugins it uses to the `.vim` directory:

        cd && git clone --recursive https://bitbucket.org/shapeshifter/config-vim.git ~/.vim

4.  Symlink `.vimrc`:

        ln -s ~/.vim/vimrc ~/.vimrc

## Updating
You can fetch the latest version of this config and all submodules by running

    cd ~/.vim && git pull && git submodule update --recursive --remote

## Adding plugins
If you want to add any other plugins, you do so by running

     cd ~/.vim && git submodule add <git repository> bundle/<plugin name>

## Requirements for syntastic
Syntastic checks the syntax of source files. For various languages, different
tools are required to be installed on the machine. The supported tools are
listed in `bundle/syntastic/syntax_checkers/`. Many of them complement each
other or are interchangeable. Requirements for some of the more popular
languages:

* C: checkpatch, gcc, make, oclint, sparse, splint, ycm
* C++: cpplint, gcc, oclint, ycm
* CSS: csslint, phpcs, prettycss
* Ruby: jruby, macruby, mri, rubocop, rubylint
* Java: checkstyle, javac
* Javascript: closurecompiler, gjslint, jshint, jslint, jsl
* JSON: jsonlint, jsonval
* Python: flake8, pep257, pep8, py3kwarn, pyflakes, pylama,
  pylint, python
* Scala: fsc, scalac
* XML / XSLT: xmllint

